const express = require("express");
const Task = require("../models/Task");
const auth = require("../middleware/authorization");

const router = express.Router();

router.post("/", auth, async (req, res) => {
  try {
    const task = new Task(req.body);
    await task.save();

    return res.send(task);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/", auth, async (req, res) => {
  try {
    const userID = req.user._id;
    const products = await Task.find({user: userID});

    res.send(products);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.put("/:id", auth, async (req, res) => {
  try {
    const taskID = req.params.id;
    const updatedTask = await Task.findOneAndUpdate({_id: taskID}, req.body, {runValidators: true, new: true}, (error) => {
      if (error) {
        const errorMessage = error.errors.status.properties.message;
        const permittedValues = error.errors.status.properties.enumValues;

        res.status(400).send(`Error: ${errorMessage}.\nValue must be one of (${permittedValues.join(" / ")})`);
      }
    });

    res.send(updatedTask);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    const taskID = req.params.id;
    await Task.deleteOne({_id: taskID});

    res.sendStatus(204);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;